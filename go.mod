module gitlab.com/ashinnv/okomot

go 1.13

require (
	gitlab.com/ashinnv/okoframe v0.0.0-20211005151938-8faf416e29ce
	gitlab.com/ashinnv/okolog v0.0.0-20210916124216-958d7d3b1cee // indirect
	gitlab.com/ashinnv/okonet v0.0.0-20210916124258-598c1765e101
	gocv.io/x/gocv v0.28.0 // indirect
)
