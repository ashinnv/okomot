package main

import(
	"gitlab.com/ashinnv/okonet"
	"gitlab.com/ashinnv/okoframe"
)

struct colorThreshold{
	R uint16
	B uint16
	G uint16
	A uint16
}

func main(){

	stageOne   := make(chan okoframe.Frame)
	finalStage := make(chan okoframe.Frame)

	for i := 0; i < motionDetectorThreads;i++{
		go moFin(stageOne, finalstage)
	}

	go okonet.FrameListener(":8082", stageOne)
	go okonet.FrameSender("localhost:8083", finStage)
}

func moFin(stageOne chan okoframe.Frame, finalStage, okoframe.Frame){

	var workingFrame okoframe.Frame
	var cycle map[string]int 
	var refreshCount map[string]int //uniqcam is the identification of a camera by its username, camid and unitid. Refreshcount is how often we update the comparison image
	var compFrames map[string]okoframe.Frame //The frame that the current frame gets compared against to find motion

	//Set up the compFrame 
	compFrame = <- stageOne 

	for{
		workingFrame = <- stageOne
		workingFrame.MotionPercentChange = getPixDiffCount(workingFrame, compFrames, 3)

		if refreshCount[getID(workingFrame)] % cycle[getID(workingFrame)] == 0{
			compFrame = workingFrame
		}

		cycle[getID(workingFrame)] +=1
	
		finalStage <- workingFrame
	}
}

//Count number of non-iedntical pixels between the two frames. TODO: Add thresholds.
func getPixDiffCount(workingFrame okoframe.Frame, compFrames map[uniqCam], /*thresh colorThreshold*/ thresh int) uint {

	var retDat uint = 0

	frame,checkmap := compFrames[getID(workingFrame)]
	if checkMap == false{
		return 0
	}

	compDat := frame.ImDat
	frDat := workingFrame.ImDat

	if len(compDat) != len(frDat){
		fmt.Println("Data lens were different ingitpixdiffcount")
		return 0
	}

	for i:=0; i < len(compDat); i++{
		if diff(int(compDat[i]), int(frDat[i])) > thresh{
			retDat += 1
		}
	}

	return retDat / compFrame.Channels
}


//Simple function to get a unique string to identify frames from a particular camera on a particular unit
func getID(input okoframe.Frame)string{

	var output string = ""

	output = output + input.UserName + input.CamId + input.UnitID

	return output
}


//Simple uitility function to get absolute difference between color channel pixel values in a pixel
func diff(a int, b int) int {
	if a > b{
		return a - b
	}

	return b - a
	
}